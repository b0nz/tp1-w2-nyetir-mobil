<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Stir Mobil</title>
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM"
            crossorigin="anonymous"
        />
        <style>
            html {
                scroll-behavior: smooth;
            }
            .navbar {
                padding: 20px 0;
            }
            .hero {
                min-height: 80vh;
                padding: 100px 0;
                background: linear-gradient(
                        rgba(0, 0, 0, 0.2),
                        rgba(0, 0, 0, 0.2)
                    ),
                    url("https://images.unsplash.com/photo-1475782944331-0ea8c9089a6a?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2500&q=80");
                background-size: cover;
                background-position: center;
            }
        </style>
        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz"
            crossorigin="anonymous"
        ></script>
    </head>
    <body>
        <!-- Nav -->
        <nav class="navbar navbar-expand-lg bg-body-tertiary sticky-top">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">StirMobil</a>
                <button
                    class="navbar-toggler"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div
                    class="collapse navbar-collapse"
                    id="navbarSupportedContent"
                >
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="#home">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#paket">Paket</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#instructor"
                                >Instructor</a
                            >
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#jadwal">Jadwal</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#about">About</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Hero Section -->
        <section id="home" class="hero d-flex align-items-center text-center">
            <div class="container">
                <h1
                    class="display-4 font-bold"
                    style="color: white; font-weight: 700"
                >
                    Belajar Mengemudi, Mudah dan Aman!
                </h1>
                <p class="lead" style="color: white; font-weight: 400">
                    Kami siap membimbing Anda untuk menguasai kemampuan
                    mengemudi yang handal dan percaya diri. Nikmati proses
                    belajar yang menyenangkan dengan instruktur profesional
                    kami. Pilih jadwal dan paket yang sesuai dengan kebutuhan
                    Anda. Kami menunggu partisipasi Anda.
                </p>
                <a
                    href="#paket"
                    class="btn btn-primary btn-lg"
                    style="margin-top: 20px; padding: 16px"
                    >Lihat Paket Belajar</a
                >
            </div>
        </section>

        <!-- Paket Section -->
        <section id="paket" class="my-5" style="padding: 150px 0">
            <div class="container">
                <h2 class="text-center">Paket Jasa Yang Ditawarkan</h2>
                <p class="lead text-center">
                    Kami menawarkan berbagai paket pembelajaran mengemudi yang
                    dirancang untuk memenuhi kebutuhan Anda.
                </p>

                <div class="row">
                    <div class="col-md-4">
                        <div class="card mb-4">
                            <div class="card-body">
                                <h5 class="card-title">Paket Dasar</h5>
                                <p class="card-text">
                                    Paket ini ditujukan untuk pemula yang ingin
                                    mempelajari dasar-dasar mengemudi. Anda akan
                                    mendapatkan pelatihan teori dan praktik
                                    selama 10 jam.
                                </p>
                                <a href="#" class="btn btn-primary"
                                    >Daftar Paket Dasar</a
                                >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card mb-4">
                            <div class="card-body">
                                <h5 class="card-title">Paket Lanjutan</h5>
                                <p class="card-text">
                                    Paket ini ditujukan untuk pengemudi dengan
                                    sedikit pengalaman yang ingin memperdalam
                                    kemampuan mereka. Anda akan mendapatkan
                                    pelatihan teori dan praktik selama 20 jam.
                                </p>
                                <a href="#" class="btn btn-primary"
                                    >Daftar Paket Lanjutan</a
                                >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card mb-4">
                            <div class="card-body">
                                <h5 class="card-title">Paket Profesional</h5>
                                <p class="card-text">
                                    Paket ini ditujukan untuk pengemudi
                                    berpengalaman yang ingin mengasah kemampuan
                                    mereka. Anda akan mendapatkan pelatihan
                                    teori dan praktik intensif selama 30 jam.
                                </p>
                                <a href="#" class="btn btn-primary"
                                    >Daftar Paket Profesional</a
                                >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Instructors -->
        <section id="instructor" class="my-5" style="padding: 100px 0">
            <div class="container">
                <h2 class="text-center">Pengajar Kami</h2>
                <p class="lead text-center">
                    Instruktur kami berpengalaman dan berdedikasi untuk
                    memberikan pembelajaran berkualitas terbaik bagi Anda.
                </p>

                <div class="row justify-content-md-center gt-0">
                    <div
                        class="col-md-4 text-center d-flex justify-content-center align-items-center flex-column"
                    >
                        <img
                            src="https://xsgames.co/randomusers/assets/avatars/male/33.jpg"
                            class="rounded-circle mb-4"
                            alt="Foto Pak Budi"
                            width="150"
                        />
                        <h5>Pak Budi</h5>
                        <p>
                            Pak Budi memiliki lebih dari 20 tahun pengalaman
                            dalam mengajar cara mengemudi. Dia adalah ahli dalam
                            pembelajaran mobil manual.
                        </p>
                    </div>
                    <div
                        class="col-md-4 text-center d-flex justify-content-center align-items-center flex-column"
                    >
                        <img
                            src="https://xsgames.co/randomusers/assets/avatars/male/30.jpg"
                            class="rounded-circle mb-4"
                            alt="Foto Pak Andi"
                            width="150"
                        />
                        <h5>Pak Andi</h5>
                        <p>
                            Pak Andi adalah instruktur yang sangat disukai oleh
                            siswa karena metode pengajarannya yang sabar dan
                            mudah dipahami. Dia adalah ahli dalam pembelajaran
                            mobil matic.
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <!-- Jadwal Section -->
        <section id="jadwal" class="my-5" style="padding: 100px 0">
            <div class="container">
                <h2 class="text-center">Jadwal Belajar Mengemudi</h2>
                <p class="lead text-center">
                    Kami berkomitmen untuk memberikan fleksibilitas dalam
                    belajar. Pilih jadwal yang paling sesuai dengan rutinitas
                    Anda.
                </p>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Hari</th>
                                <th scope="col">Waktu</th>
                                <th scope="col">Instruktur</th>
                                <th scope="col">Jenis Kendaraan</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Senin</td>
                                <td>09:00 - 11:00</td>
                                <td>Pak Budi</td>
                                <td>Manual</td>
                                <td>
                                    <span class="badge bg-success"
                                        >Tersedia</span
                                    >
                                </td>
                            </tr>
                            <tr>
                                <td>Selasa</td>
                                <td>13:00 - 15:00</td>
                                <td>Pak Andi</td>
                                <td>Matic</td>
                                <td>
                                    <span class="badge bg-danger">Penuh</span>
                                </td>
                            </tr>
                            <tr>
                                <td>Rabu</td>
                                <td>09:00 - 11:00</td>
                                <td>Pak Budi</td>
                                <td>Manual</td>
                                <td>
                                    <span class="badge bg-success"
                                        >Tersedia</span
                                    >
                                </td>
                            </tr>
                            <tr>
                                <td>Kamis</td>
                                <td>13:00 - 15:00</td>
                                <td>Pak Andi</td>
                                <td>Matic</td>
                                <td>
                                    <span class="badge bg-success"
                                        >Tersedia</span
                                    >
                                </td>
                            </tr>
                            <tr>
                                <td>Jumat</td>
                                <td>09:00 - 11:00</td>
                                <td>Pak Budi</td>
                                <td>Manual</td>
                                <td>
                                    <span class="badge bg-success"
                                        >Tersedia</span
                                    >
                                </td>
                            </tr>
                            <tr>
                                <td>Sabtu</td>
                                <td>13:00 - 15:00</td>
                                <td>Pak Andi</td>
                                <td>Matic</td>
                                <td>
                                    <span class="badge bg-danger">Penuh</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>

        <!-- About -->
        <section id="about" class="my-5">
            <div class="container">
                <h2 class="text-center">Tentang Kami</h2>
                <p class="lead text-center">
                    Kami adalah perusahaan profesional yang berdedikasi untuk
                    memandu Anda dalam belajar mengemudi.
                </p>

                <div class="row" style="margin-top: 80px;">
                    <div class="col-lg-6">
                        <h3 class="text-center">Tentang Perusahaan</h3>
                        <p>
                            Sejak berdirinya sekolah kami pada tahun 2000, kami
                            telah membantu ribuan siswa untuk menjadi pengemudi
                            yang aman dan percaya diri. Dengan instruktur yang
                            berpengalaman dan metode pengajaran yang terbukti
                            efektif, kami berkomitmen untuk memberikan layanan
                            terbaik bagi semua siswa kami.
                        </p>

                        <p>
                            Kami menawarkan berbagai paket pembelajaran, baik
                            untuk pemula maupun pengemudi berpengalaman yang
                            ingin meningkatkan keterampilan mereka. Setiap siswa
                            kami mendapatkan pelajaran yang disesuaikan dengan
                            kebutuhan dan kecepatan belajar mereka sendiri. Kami
                            percaya bahwa setiap individu unik dan berhak
                            mendapatkan pendekatan yang paling efektif untuk
                            belajar mengemudi.
                        </p>

                        <p>
                            Dengan berbagai jenis kendaraan, baik manual maupun
                            matic, kami dapat memenuhi berbagai kebutuhan dan
                            preferensi siswa kami. Lokasi kami yang strategis
                            juga memudahkan siswa untuk datang dan belajar.
                        </p>
                    </div>
                    <div class="col-lg-6">
                        <h3 class="text-center">Kontak dan Lokasi</h3>
                        <div>Anda dapat menghubungi kami melalui</div>
                        <div>Nomor Telepon : (123) 456-7890</div>
                        <div>Email : info@setirmobil.com</div>
                        <div>Alamat : Jalan Raya No. 123, Kota ABC.</div>

                        <div class="map-responsive" style="margin-top: 20px;">
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.4876847418416!2d106.81861141427331!3d-6.175394395522184!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f379f6752eb3%3A0x100e7be2ed15a0a0!2sMonas%2C+Gambir%2C+Central+Jakarta+City%2C+Jakarta%2C+Indonesia!5e0!3m2!1sen!2sus!4v1453788292505"
                                width="600"
                                height="450"
                                frameborder="0"
                                style="border: 0"
                                allowfullscreen
                            ></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
